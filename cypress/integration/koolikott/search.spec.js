describe('tests for search', ()=>{
    beforeEach(()=>{
        cy.visit('/');
    })

    it('should find a specific topic using filters', function () {

        //inputs search topic
        cy.get('#search').type('Proovitöö')
        cy.get('[type="submit"]').click()

        //select curriculum
        cy.curriculum('Põhiharidus')
        cy.curriculum('Kunstiained')
        cy.curriculum('Muusika')

        //changes class range
        //TODO: Test is very flaky. Change should trigger on value change instead
        cy.get('.ngx-slider-pointer-min')
            .first()
            .type('{rightarrow}{rightarrow}{rightarrow}' +
                '{rightarrow}{rightarrow}{rightarrow}')
            //.invoke('attr', 'aria-valuenow', 110)
            //.trigger('change')

        //check 'Kultuuri- ja väärtuspädevus'
        cy.get('[id="key-competence-checkbox-1"]').parents('kott-checkbox').click()

        //clicks 'Heli' button
        cy.get('#musicFilterBtn').click()

        //change to requested result
        cy.get('.material-card', {timeout: 30000}).should('be.visible')
            .find('a')
            .first()
            .click()

        //assert url is valid
        cy.wait(7000)
        cy.url().should('eq', 'https://e-koolikott.ee/et/oppematerjal/31832-Proovitoo-materjal')
    });
})